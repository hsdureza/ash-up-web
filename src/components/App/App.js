import React, { Component } from 'react'
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    withRouter,
    Redirect
} from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'
import Textarea from 'react-textarea-autosize'
import './App.css'


const history = createHistory()

const forWhom = {
  name: 'for-whom',
  choices: [
    {
      id: 'for-you',
      value: 'For Yourself',
      subtext: 'You were a victim/target of an act of sexual harassment'
    },
    {
      id: 'for-a-friend',
      value: 'For a Friend',
      subtext: 'Your friend was a victim/target of an act of sexual harassment'
    }
  ]
}

const listOfLocations = {
  name: 'location',
  choices: [
    {
      id: 'inside',
      value: 'Inside UP',
      subtext: 'Happened inside University premises, stores, booths'
    },
    {
      id: 'around',
      value: 'Around UP',
      subtext: 'Happened around the University (i.e., Philcoa, Krus na Ligas, Katipunan)'
    },
    {
      id: 'outside-official',
      value: 'Outside UP (UP-related)',
      subtext: 'For a University-related event or activity outside the University (i.e., field work, research, excursion)'
    },
    {
      id: 'outside-unofficial',
      value: 'Outside UP (non-related)',
      subtext: 'Event or activity which does not involve the University or was not endorsed by the University'
    }
  ]
}

const listOfActs = {
  name: 'acts',
  addPrompt: 'Add another act',
  placeholderText: 'E.g. forced kissing',
  choices: [
      { class: `light`, value: `Stole a look at my private parts` },
      { class: `light`, value: `Stole a look at my underclothing` },
      { class: `light`, value: `Looked at me maliciously` },
      { class: `light`, value: `Looked at me with sexual attraction` },
      { class: `light`, value: `Flirted with me sexually` },
      { class: `light`, value: `Persistent attention with sexual overtones` },
      { class: `light`, value: `Told offensive sexist remarks` },
      { class: `light`, value: `Embarassed me with sexist remarks` },
      { class: `light`, value: `Insulted me with sexist remarks` },
      { class: `light`, value: `Showed sexually-offensive picture(s), material(s), graffiti` }
  ]
}

const listOfOffenders = {
  name: 'offenders',
  addPrompt: 'Add another person',
  placeholderText: 'E.g. student, professor, stranger',
  choices: [
    { class: `student`, value: 'Student' },
    { class: `employee`, value: 'Teacher' },
    { class: `employee`, value: 'Professor' },
    { class: `employee`, value: 'Faculty member' },
    { class: `employee`, value: 'Non-teaching Personnel' },
    { class: `org`, value: 'Recognized organization/society' },
    { class: `org`, value: 'Recognized fraternity/sorority' },
    { class: `otherUP`, value: 'Other UP worker' },
    { class: `stranger`, value: 'Stranger' },
    { class: `stranger`, value: 'Cannot identify' }
  ]
}

const caseTemplate = {
  'for-whom': '',
  'acts': [''],
  'acts-details': '',
  'offenders': [''],
  'offenders-details': '',
  'location': '',
  'location-details': ''
}

// const prefilled = {
//   'for-whom': 'for-a-friend',
//   'acts': ['Stole a look at my private parts', 'wow', 'Looked at me maliciously', 'wew'],
//   'acts-details': 'more acts details',
//   'offenders': ['teacher', 'student'],
//   'offenders-details': 'more offenders details',
//   'location': 'around',
//   'location-details': 'more location details'
// }

let globalState = {
  currentCase: 0,
  cases: [Object.assign({}, caseTemplate)]
}

const properAddress = (possessive = false) => {
  const currentCase = globalState.currentCase
  const listName = 'for-whom'
  let cases = globalState.cases

  return (cases[currentCase][listName] === 'for-a-friend') ? (possessive ? 'your friend\'s' : 'your friend') : (possessive ? 'your' : 'you')
}


const Header__Home = () => (
  <div className="Header__flux Header__Home">
    <li className="Header__flux Header__login">
      <Link to="/login" className="Header__loginLink Header__link">login</Link>
    </li>
  </div>
)

const Header__Default = () => {
  return (
    <div className="Header__flux Header__Default">
      <li className="Header__flux Header__back">
        <div onClick={history.goBack} className="Header__link">
          <i className="fa fa-2x fa-angle-left" aria-hidden="true"></i>
        </div>
      </li>
      <li className="Header__flux Header__homeLink">
        <Link to="/" className="Header__link">
          <i className="fa fa-2x fa-home" aria-hidden="true"></i>
        </Link>
      </li>
      <li className="Header__flux Header__titleText">ash up.</li>
      <li className="Header__flux Header__titleLogoContainer">
        <div className="Header__titleLogo"></div>
      </li>
    </div>
  )
}

const Header = withRouter((props) => {
  const state = props.state
  return (
    <nav className="App__Header">
      <Switch>
        <Route exact path="/" component={Header__Home} loggedInUser={state.loggedInUser}/>
        <Route component={Header__Default}/>
      </Switch>
    </nav>
  )
})

const Body__Home = () => (
  <div className="Body__flux Body__Home">
    <div className="Home__oash">Office of Anti-Sexual Harassment | UP Diliman</div>
    <div className="Home__ashUpLogo"></div>
    <div className="Home__ashUpTitle">ash up.</div>
    <div className="Home__buttons">
      <ul className="Home__buttonList">
        <li className="Home__button">
          <Link to="/consult" className="Home__consultButton">Consult</Link>
        </li>
        <li className="Home__button">
          <Link to="/about" className="Home__auxButton Home__aboutButton">About the App</Link>
        </li>
        <li className="Home__button">
          <Link to="/manage" className="Home__auxButton Home__adminButton">Manage Cases</Link>
        </li>
      </ul>
    </div>
  </div>  
)

const Body__About = withRouter((props) => (
  <div className="Body__default Body__About">
    <h1 className="About__titleAbout">About the App</h1>
    <p className="About__overview">Designed to complement the new <b>Anti-Sexual Harassment Code</b> (ASH Code), as well as guide the users to what possible actions one can undertake in situations related to sexual harassment, <b>ash up.</b> aims to be a working prototype under ten (10) weeks, under the guidance of the <b>Office of Anti-Sexual Harassment</b> (OASH) of <b>UP Diliman</b>, in partial fulfillment of the requirements for the subject <b>CoE 134 - Computer Systems Engineering II</b>.</p>
    <a href="http://www.up.edu.ph/up-anti-sexual-harassment-code/" className="Body__auxButton"><i className="fa fa-external-link fa-lg" aria-hidden="true"></i>Read the ASH Code</a>
    <h1 className="About__titleTeam">Team members</h1>
    <ul className="About__teamList">
      <li className="About__teamMember"><b>Charles Alba</b> (Charles) is the team’s project manager and designer. As a project manager he overlooks on the entire project and guides the project through the next steps with the consensus of all members. As a designer he creates the visuals and look of the mobile application.</li>
      <li className="About__teamMember"><b>Justine Beaño</b> (Justine) is the team’s front-end developer. As a front-end developer, he codes what Charles does into Intel XDK, and links the views together through links and actions.</li>
      <li className="About__teamMember"><b>Hans Dureza</b> (Hans) is the team’s back-end developer. As a back-end developer, he manages user authentication, profiles, reported cases, and database management, as well as provide (or remove) access to the users involved and the administrators (OASH).</li>
      <li className="About__teamMember"><b>Rommel Monsayac</b> (Rommel) is the team’s customer contact. As the customer contact he handles all client-related communications, sets meetings, and will be the point person for future conversations, questions, and comments about the project.</li>
    </ul>
  </div>
))

class RadioChoices extends Component {
  constructor (props) {
    super(props)

    this.state = {
      choicesList: this.props.choicesList,
      currentCase: globalState.currentCase,
      listName: this.props.choicesList.name,
      cases: globalState.cases
    }
    
    this.renderChoices = this.renderChoices.bind(this)
  }


  renderChoices () {
    const isChecked = (choice) => {
      return this.state.cases[this.state.currentCase][this.state.listName] === choice.id
    }

    const changeChecked = (e) => {
      const checked = e.target.id
      const choicesList = this.props.choicesList
      const currentCase = globalState.currentCase
      const listName = choicesList.name
      let cases = globalState.cases
      
      cases[currentCase][listName] = checked
      this.setState({
        cases: cases
      })

      globalState.cases = cases
    }

    return this.state.choicesList.choices.map((choice, index) => (
      <div key={index} className="Body__radio">
        <input type="radio" name={this.state.listName} id={choice.id} checked={isChecked(choice)} onChange={changeChecked}/>
        <label htmlFor={choice.id}>
          {choice.value}
          {choice.subtext && choice.subtext.length > 0 && <p className="subtext">{choice.subtext}</p>}
        </label>
      </div>
    ))
  }

  render () {
    return (
      <div className="Body__inputGroup">
        {this.renderChoices()}
      </div>
    )
  }
}

const Body__ConsultDisclaimer = withRouter((props) => {
  return (
    <div className="Body__default Body__Consult">
      <h1>Before We Begin</h1>
      <p>
        <b>ash up. </b>gives advices in line with the University's <b>ASH Code</b> by asking a series of situation-related questions.</p>
        <p>However, submitting data through this app <b>does not guarantee action from the OASH</b>—a personal consultation is still necessary for further action.</p>
        <p>Regardless, <b>we take your privacy seriously</b>, and we will treat all information sent through the app as confidential.</p>
        <p>You may also leave any question unanswered, however the advice we will be giving you may not be as specific or will lack some information.
      </p>
      <h1>Who are you answering this for?</h1>
      <RadioChoices choicesList={forWhom} />
      <Link to="/consult/acts" className="Body__auxButton"><i className="fa fa-lg fa-chevron-right" aria-hidden="true"></i>Next</Link>
    </div>
  )
})

class AutoCompleteOption extends Component {
  constructor (props) {
    super(props)

    const options = this.props.options
    const newCurrent = this.props.value
    const sortingFunction = (a, b) => {
      const first = a.toLowerCase()
      const second = b.toLowerCase()
      const test = newCurrent.toLowerCase()
      if (first.indexOf(test) < second.indexOf(test)) {
        return -1
      } else if (first.indexOf(test) > second.indexOf(test)) {
        return 1
      } else return 0
    }

    this.state = {
      current: this.props.value,
      options: this.props.options,
      suggestions: this.getSuggestions(options, newCurrent, sortingFunction)
    }
    
    this.handleInputChange = this.handleInputChange.bind(this)
    this.changeCurrent = this.changeCurrent.bind(this)
    this.updateSuggestions = this.updateSuggestions.bind(this)
    this.updateSuggestions = this.updateSuggestions.bind(this)
  }

  handleInputChange (e) {
    const options = this.state.options
    const newCurrent = e.target.value
    
    const sortingFunction = (a, b) => {
      const first = a.toLowerCase()
      const second = b.toLowerCase()
      const test = newCurrent.toLowerCase()
      if (first.indexOf(test) < second.indexOf(test)) {
        return -1
      } else if (first.indexOf(test) > second.indexOf(test)) {
        return 1
      } else return 0
    }

    this.setState({
      current: newCurrent,
      suggestions: this.getSuggestions(options, newCurrent, sortingFunction)
    })
    
    this.props.updateList(newCurrent, this.props.identifier)
  }

  getSuggestions (options, newCurrent, sortingFunction) {
    return options.filter(option => (newCurrent !== option) && option.toLowerCase().includes(newCurrent.toLowerCase())).sort(sortingFunction)
  }

  changeCurrent (e) {
    const newCurrent = e.target.innerText

    this.setState({
      current: newCurrent,
      suggestions: []
    })

    this.props.updateList(newCurrent, this.props.identifier)
  }

  updateSuggestions () {
    const suggestions = this.state.suggestions
    return suggestions.map((suggestion, index) => {
      return (
        <li key={index} onClick={this.changeCurrent}>{suggestion}</li>
      )
    })
  }

  render () {
    return (
      <div className="Body__AutoCompleteOption">
        <div className="AutoCompleteOption__textField">
          <input type="text" value={this.state.current} onChange={this.handleInputChange} placeholder={this.props.placeholderText}/>
        </div>
          <div className="AutoCompleteOption__suggestions">
            {this.state.current.length > 0 &&
              <ul>
                {this.updateSuggestions()}
              </ul>}
        </div>
      </div>
    )
  }
}

class AutoCompleteGroup extends Component {
  constructor (props) {
    super(props)

    const currentCase = globalState.currentCase
    const property = this.props.choicesList
    let cases = globalState.cases
    
    this.state = {
      userList: cases[currentCase][property.name]
    }
    
    this.renderList = this.renderList.bind(this)
    this.addAnother = this.addAnother.bind(this)
    this.updateList = this.updateList.bind(this)
  }

  renderList () {
    const userList = this.state.userList
    const options = this.props.choicesList.choices.map(x => x.value)
    const placeholderText = this.props.choicesList.placeholderText
    return userList.map((entry, index) => <AutoCompleteOption key={index} value={entry} identifier={index} options={options} updateList={this.updateList} placeholderText={placeholderText}/>)
  }

  addAnother () {
    const userList = this.state.userList
    const newUserList = [...userList, '']

    this.setState({
      userList: newUserList
    })

    const currentCase = globalState.currentCase
    const property = this.props.choicesList
    let cases = globalState.cases

    cases[currentCase][property.name] = newUserList
    globalState.cases = cases
  }

  updateList (act, index) {
    let userList = this.state.userList
    userList[index] = act
    this.setState({
      userList: userList
    })

    const currentCase = globalState.currentCase
    const property = this.props.choicesList
    let cases = globalState.cases

    cases[currentCase][property.name] = userList
    globalState.cases = cases
  }

  render () {
    return (
      <div className="Body__AutoCompleteGroup">
        {this.renderList()}
        <div className="AutoCompleteGroup__addAnother" onClick={this.addAnother}>
          <i className="fa fa-lg fa-plus" aria-hidden="true"></i>{this.props.choicesList.addPrompt}
        </div>
      </div>
    )
  }
}

class CustomTextarea extends Component {
  constructor (props) {
    super(props)
    this.state = {
      for: this.props.for,
      currentCase: globalState.currentCase,
      cases: globalState.cases,
      value: globalState
    }

    this.changeChecked = this.changeChecked.bind(this)
  }

  changeChecked (e) {
    const value = e.target.value
    const currentCase = globalState.currentCase
    const property = this.props.for
    let cases = globalState.cases

    cases[currentCase][property] = value
    this.setState({
      cases: cases
    })

    globalState.cases = cases
  }

  render () {
    return (
      <Textarea value={this.state.cases[this.state.currentCase][this.state.for]} onChange={this.changeChecked}></Textarea>
    )
  }
}
      // <AutoCompleteGroup options={listOfActs.map(x => x.act)} addPrompt="Add another act" placeholderText="E.g. forced kissing"/>

const Body__ConsultActs = withRouter((props) => {
  return (
    <div className="Body__default Body__Consult">
      <h1>What happened?</h1>
      <p>Sexual harassment is unwanted, unwelcome, uninvited behavior of a sexual nature or inappropriate sexual advances or offensive remark about a person's sex, sexual orientation, or gender identity.</p>
      <p><b>What were the acts committed against {properAddress()}?</b></p>
      <p className="subtext">Suggestions will pop up as you type. Please select the nearest equivalent from the list, if applicable.</p>
      <AutoCompleteGroup choicesList={listOfActs} />
      <p><b>Are there more details?</b></p>
      <p className="subtext">How did {properAddress()} respond? When did it happen?</p>
      <div className="Consult__textArea">
        <CustomTextarea for="acts-details"/>
      </div>
      <Link to="/consult/offenders" className="Body__auxButton"><i className="fa fa-lg fa-chevron-right" aria-hidden="true"></i>Next</Link>
    </div>
  )
})
      // <AutoCompleteGroup options={offenderTypes} addPrompt="Add another offender" placeholderText="E.g. student, professor, stranger"/>

const Body__ConsultOffenders = withRouter((props) => {
  return (
    <div className="Body__default Body__Consult">
      <h1>Who did it?</h1>
      <p>The ASH Code covers all members of the UP community. This includes students, faculty, non-teaching personnel, organizations, and other UP workers.</p>
      <p><b>How would you identify {properAddress(true)} offender(s)?</b></p>
      <p className="subtext">Have one answer per offender.</p>
      <AutoCompleteGroup choicesList={listOfOffenders}/>
      <p><b>Are there more details?</b></p>
      <p className="subtext">What's their name(s)? Their gender(s)? From which college? From which department?</p>
      <div className="Consult__textArea">
        <CustomTextarea for="offenders-details"/>
      </div>
      <Link to="/consult/location" className="Body__auxButton"><i className="fa fa-lg fa-chevron-right" aria-hidden="true"></i>Next</Link>
    </div>
  )
})

const Body__ConsultLocation = withRouter((props) => {
  return (
    <div className="Body__default Body__Consult">
      <h1>Where did it happen?</h1>
      <p>The ASH Code covers cases that happened within the University and during events tied to official University matters. For cases outside this scope, the OASH can <b>refer {properAddress()} to the appropriate office or unit</b> than can help you.</p>
      <p><b>Where, in relation to UP, did it happen?</b></p>
      <RadioChoices choicesList={listOfLocations}/>
      <p><b>Are there more details?</b></p>
      <p className="subtext">Where exactly did it happen?</p>
      <div className="Consult__textArea">
        <CustomTextarea for="location-details"/>
      </div>
      <Link to="/consult/analysis" className="Body__auxButton"><i className="fa fa-lg fa-chevron-right" aria-hidden="true"></i>Next</Link>
    </div>
  )
})

const Body__ConsultAnalysis = withRouter((props) => {
  const currentCase = globalState.currentCase
  const cases = globalState.cases
  const acts = cases[currentCase]['acts']
  const actsDetails = cases[currentCase]['acts-details']
  const offenders = cases[currentCase]['offenders']
  const offendersDetails = cases[currentCase]['offenders-details']
  const location = cases[currentCase]['location']
  const locationDetails = cases[currentCase]['location-details']

  const getOffenderClasses = () => {
    return offenders.map(offender => listOfOffenders.choices.find(choice => choice.value.toLowerCase() === offender.toLowerCase())).map(x => x ? x.class : 'custom').filter((value, index, self) => self.indexOf(value) === index)
  }

  const getActClasses = () => {
    return acts.map(act => listOfActs.choices.find(choice => choice.value.toLowerCase() === act.toLowerCase())).map(x => x ? x.class : 'custom').filter((value, index, self) => self.indexOf(value) === index)
  }

  const getGravityOfActs = () => {
    return getActClasses().map(x => {
      switch (x) {
        case 'light': return 1
        case 'less-grave': return 2
        case 'grave': return 3
        default: return 0
      }
    })
  }

  const getHighestGravity = () => {
    return Math.max(...getGravityOfActs())
  }

  const plurality = (type = 's') => {
    if (type === 'a') return acts.filter(x => x).length > 1 ? '' : 'a'
    else if (type === 'be') return acts.filter(x => x).length > 1 ? 'are' : 'is'
    else return acts.filter(x => x).length > 1 ? 's' : ''
  }

  const determineGravity = () => {
    if (getHighestGravity() === 1) {
      return (
        <div>
          <h2>Highest Act Gravity</h2>
          <p>The act{plurality()} presented {plurality('be')} classified as {plurality('a')} <b>light offense{plurality()}</b>.</p>
        </div>
      )
    } else if (getHighestGravity() === 2) {
      return (
        <div>
          <h2>Highest Act Gravity</h2>
          <p>The act{plurality()} presented {plurality('be')} classified as {plurality('a')} <b>less grave offense{plurality()}</b>.</p>
        </div>
      )
    } else if (getHighestGravity() === 3) {
      return (
        <div>
          <h2>Highest Act Gravity</h2>
          <p>The act{plurality()} presented {plurality('be')} classified as {plurality('a')} <b>grave offense{plurality()}</b>.</p>
        </div>
      )
    }
  }

  const determinePenalty = (offenderClass, index) => {
    if (offenderClass === 'student' && getHighestGravity() === 1) {
      return (
        <div className="penalty" key={index}>
          <p>Student(s) get(s) <b>reprimanded</b> or undergo <b>community service</b> (&lt; 30 hours) for the first offense.</p>
          <p>For the succeeding offenses, the student may get <b>suspended</b> or <b>face expulsion</b>.</p>
        </div>
      )
    } else if (offenderClass === 'student' && getHighestGravity() === 2) {
      return (
        <div className="penalty" key={index}>
          <p>Student(s) should undergo <b>community service</b> (&lt; 60 hours) for the first offense.</p>
          <p>For the succeeding offenses, the student may get <b>suspended</b> or <b>face expulsion</b>.</p>
        </div>
      )
    } else if (offenderClass === 'student' && getHighestGravity() === 3) {
      return (
        <div className="penalty" key={index}>
          <p>Student(s) will be <b>suspended</b> or <b>face expulsion</b>.</p>
        </div>
      )
    } else if (offenderClass === 'student') {
      return (
        <div className="penalty" key={index}>
          <p>Depending on the gravity of the situation, student(s) will either be <b>reprimanded</b>, <b>suspended</b>, or <b>face expulsion</b>.</p>
        </div>
      )
    } else if (offenderClass === 'employee' && getHighestGravity() === 1) {
      return (
        <div className="penalty" key={index}>
          <p>Employee(s) get(s) <b>reprimanded</b> or <b>suspended</b> (1 month &lt; duration &lt; 6 months) for the first offense.</p>
          <p>For the succeeding offenses, the UP employee may get <b>fined</b>, <b>suspended</b> or <b>face dismissal</b>.</p>
        </div>
      )
    } else if (offenderClass === 'employee' && getHighestGravity() === 2) {
      return (
        <div className="penalty" key={index}>
          <p>Employee(s) get(s) <b>suspended</b> (6 months &lt; duration &lt; 1 year) for the first offense.</p>
          <p>For the succeeding offenses, the UP employee will <b>face dismissal</b>.</p>
        </div>
      )
    } else if (offenderClass === 'employee' && getHighestGravity() === 3) {
      return (
        <div className="penalty" key={index}>
          <p>Employee(s) will be <b>dismissed from the University</b>.</p>
        </div>
      )
    } else if (offenderClass === 'employee') {
      return (
        <div className="penalty" key={index}>
          <p>Depending on the gravity of the situation, employee(s) will either be <b>reprimanded</b>, <b>suspended</b>, or <b>face dismissal</b>.</p>
        </div>
      )
    } else if (offenderClass === 'org') {
      return (
        <div className="penalty" key={index}>
          <p>Depending on the gravity of the situation, organization(s) will either face <b>suspension of privileges</b>, or <b>be non-recognized</b>.</p>
          <p><b>Officers (and involved members)</b> will be penalized as well.</p>
          <p>The corrective suspension of a month or less may be converted into a like period of community service.</p>
        </div>
      )
    } else if (offenderClass === 'otherUP') {
      return (
        <div className="penalty" key={index}>
          <p>Other UP Workers shall be proceeded against in <b>accordance with the provisions</b> of their contract with the University.</p>
        </div>
      )
    }
  }

  const informalProcedure = () => {
    const capitalizeFirst = (string) => string.charAt(0).toUpperCase() + string.slice(1)

    if (getHighestGravity() === 1) {
      return (
        <div>
          <p>Because of this, {properAddress()} and the accused may go through the <b>informal procedure</b>.</p>
          <p>{capitalizeFirst(properAddress())} may also opt to <b>formally report</b> this case to the OASH.</p>
          <div className="ConsultAnalysis__block">
            <h2>Informal Procedure</h2>
            <p>An <b>Alternate Dispute Resolution</b> (ADR) may be chosen as a course of action instead of going through the formal procedures.</p>
            <p><b>Involving both parties</b>, the informal procedure aims to resolve conflict and provide support services such as <b>counseling</b>, <b>administrative protection orders</b>, <b>shelter</b>, and <b>rehabilitative measures</b>.</p>
            <p>Complaints settled through ADR shall <b>at all times be with the assistance</b> of the OASH.</p>
          </div>
        </div>
      )
    } else {
      return (
        <div>
          <p>Considering the highest gravity, {properAddress()} may opt to <b>formally report</b> this case to the OASH.</p>
        </div>
      )
    }
  }

  const formalProcedure = () => {
    return (
      <div className="ConsultAnalysis__block">
        <h2>Formal Procedure</h2>
        <p>The report must be <b>written</b>, <b>signed</b> by {properAddress()}, and <b>notarized</b>.</p>
        <p>Note that the person(s) you're filing a case against <b>will be notified</b> and will have to submit to the case proceedings as well.</p>
        <p>For convenience, we are showing your previous inputs down below.</p>
        <h2>Penalties</h2>
        <p>The corrective measure(s) listed below are applicable if the case undergoes the <b>formal procedure</b>.</p>
        {getOffenderClasses().map((offender, index) => determinePenalty(offender, index))}
        <p>In addition, a <b>written or oral apology</b>, <b>counseling</b>, <b>community service</b>, and other appropriate support may be adopted.</p>
        <h2>Details of the Case</h2>
        <div className="ConsultAnalysis__details">
          <h3>Filing a Case Against</h3>
          {offenders.filter(x => x).map((offender, index) => {
            return (<li key={index}>{offender}</li>)
          })}
          {offendersDetails.length > 0 &&
            <div> 
              <h3>Filing a Case Against (details)</h3>
              <li>{offendersDetails}</li>
            </div>
          }
          <h3>Commited Sexual Harassment Act{plurality()}</h3>
          {acts.filter(x => x).map((act, index) => {
            return (<li key={index}>{act}</li>)
          })}
          {actsDetails.length > 0 &&
            <div> 
              <h3>Commited Sexual Harassment Act{plurality()} (details)</h3>
              <li>{actsDetails}</li>
            </div>
          }
          <h3>Location of Act{plurality()}</h3>
          <li>{listOfLocations.choices.find(x => x.id === location) && listOfLocations.choices.find(x => x.id === location).value}</li>
          {locationDetails.length > 0 &&
            <div> 
              <h3>Location of Act{plurality()} (details)</h3>
              <li>{locationDetails}</li>
            </div>
          }
        </div>
      </div>
    )
  }

  const validResponse = () => {   
    return (
      <div className="ConsultAnalysis__block">
        {determineGravity()}
        {informalProcedure()}
        {formalProcedure()}
      </div>
    )
  }

  const checkScope = () => {
    const offenderClasses = getOffenderClasses()

    if (location === 'outside-unofficial') {
      return (
        <div className="ConsultAnalysis__block">
          <h2>Out of scope</h2>
          <p>Unfortunately, <b>events or activities not related to the University</b> are <b>outside the ASH Code's scope</b>.</p>
          <p>You may still <b>personally consult with the OASH</b> for further consultation, as they can <b>refer {properAddress()} to the appropriate office(s) or unit(s)</b> that can provide support.</p>
          <p>To know more, please provide more information, or read the ASH Code.</p>
        </div>
      )
    } else if (offenderClasses.length === 1 && offenderClasses[0] === 'custom') {
      return (
        <div className="ConsultAnalysis__block">
          <h2>Out of scope</h2>
          <p>Unfortunately, <b>strangers</b> (until proven to be part of the UP community) are <b>outside the ASH Code's scope</b>.</p>
          <p>You may still <b>personally consult with the OASH</b> for further consultation, as they can <b>refer {properAddress()} to the appropriate office(s) or unit(s)</b> that can provide support.</p>
          <p>To know more, please provide more information, or read the ASH Code.</p>
        </div>
      )
    } else {
      return validResponse()
    }
  }

  return (
    <div className="Body__default Body__Consult">
      <h1>Analysis</h1>
        {checkScope()}
        <br/>
        <a href="http://www.up.edu.ph/up-anti-sexual-harassment-code/" className="Body__auxButton"><i className="fa fa-external-link fa-lg" aria-hidden="true"></i>Read the ASH Code</a>
    </div>
  )
})

class Body__ConsultManage extends Component {
  constructor (props) {
    super(props)

    this.state = {
      cases: globalState.cases
    }

    this.renderList = this.renderList.bind(this)
    this.addConsult = this.addConsult.bind(this)
    this.updateCases = this.updateCases.bind(this)
    this.removeCase = this.removeCase.bind(this)
    this.goConsult = this.goConsult.bind(this)
  }

  renderList () {
    return (
      this.state.cases.map((x, i) => {
        return (
          <div className="caseContainer" key={i}>
            <li><Link to="/consult/disclaimer" onClick={this.goConsult} data-index={i}>Consultation {i + 1}</Link></li>
            <p onClick={this.removeCase} data-index={i}><i className="fa fa-lg fa-times-circle" aria-hidden="true" onClick={this.removeCase} data-index={i}></i></p>
          </div>
        )
      })
    )
  }

  addConsult (e) {
    let newCases = [...this.state.cases, Object.assign({}, caseTemplate)]
    this.updateCases(newCases)
  }

  removeCase (e) {
    let newCases = this.state.cases
    newCases.splice(e.target.dataset.index, 1)
    this.updateCases(newCases)
  }

  goConsult (e) {
    globalState.currentCase = e.target.dataset.index
  }

  updateCases (newCases) {
    this.setState({
      cases: newCases
    })

    globalState.cases = newCases
  }

  render () {
    return (
      <div className="Body__default Body__About">
        <h1>Manage Consultations</h1>
        <p>You have the following consultations.</p>
        {this.renderList()}
        <div className="addConsult" onClick={this.addConsult}>
          <i className="fa fa-lg fa-plus" aria-hidden="true"></i>Add a consultation
        </div>
      </div>
    )
  }
}

const Body__Consult = withRouter((props) => (
  <Switch>
    <Route exact path="/consult" component={Body__ConsultManage}/>
    <Route exact path="/consult/disclaimer" component={Body__ConsultDisclaimer}/>
    <Route exact path="/consult/acts" component={Body__ConsultActs}/>
    <Route exact path="/consult/offenders" component={Body__ConsultOffenders}/>
    <Route exact path="/consult/location" component={Body__ConsultLocation}/>
    <Route exact path="/consult/analysis" component={Body__ConsultAnalysis}/>
    <Route render={()=> (<Redirect to="/"/>)}/>
  </Switch>
))

const Body__Construction = withRouter((props) => (
  <div className="Body__default Body__About">
    <h1>To Follow</h1>
    <p>Please go back.</p>
  </div>
))

const Body__Default = withRouter((props) => (
  <Switch>
    <Route path="/about" component={Body__About}/>
    <Route path="/consult" component={Body__Consult}/>
    <Route path="/manage" component={Body__Construction}/>
    <Route path="/login" component={Body__Construction}/>
    <Route render={()=> (<Redirect to="/"/>)}/>
  </Switch>
))

const Body = withRouter((props) => (
  <section className="App__Body">
    <Switch>
      <Route exact path="/" component={Body__Home}/>
      <Route component={Body__Default}/>
    </Switch>
  </section>
))

class App extends Component {
  constructor () {
    super()
    this.state = {
      loggedInUser: null
    }
  }

  render() {
    return (
      <Router history={history}>
        <div className="App">
          <Header state={this.state}/>
          <Body state={this.state}/>
        </div>
      </Router>
    )
  }
}

export default App
