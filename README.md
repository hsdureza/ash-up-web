# ash up. (web prototype)

## How to use
If you want to run this locally, you may do so by following these steps.
1. Make sure you have [git](https://git-scm.com/) on your computer.
2. Make sure you have [node.js](https://nodejs.org/) on your computer.
3. Clone this repository.
```
git clone git@gitlab.com:charlesjosh/ash-up-web.git
```
4. `cd` to the repository.
```
cd ash-up-web
```
5. Run `npm install` then wait.
6. Run `npm start`.
7. A browser will appear with the website loaded. Yay!